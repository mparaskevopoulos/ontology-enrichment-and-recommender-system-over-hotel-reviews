# Ontology enrichment and recommender system over hotel reviews

This repository was built as a part of the semester project for Artificial Intelligence and Data Analysis course.

Contributors:
- Marios Paraskevopoulos (03002922)
- Konstantina-Maria Giannakopoulou (03003046)

Our analysis is based on the [515K Hotel Reviews Data in Europe](https://www.kaggle.com/jiashenliu/515k-hotel-reviews-data-in-europe). The initial dataset is enriched through various data sources and knowledge bases, like Wikidata, GeoNames, Wikipedia, as well as by web scraping popular travel and booking platforms and by performing sentiment analysis on the retrieved reviews. The enriched dataset is used to build a RDF knowledge graph and an ontology, which is finally merged with other state-of-the-art domain-specific ontologies. A search mechanism and a recommender system are developed by submitting SPARQL queries to the final ontology, accompanied by the respective GUIs.

## Prerequisites

The whole implementation is provided in a Jupyter notebook. The code is written in Pyhton 3 and contains embedded SPARQL queries.

Several libraries need to be installed, including:
- the pycountry library, to retrieve countries codes: `pip install pycountry`
- the geopy library, to retrieve data from GeoNames: `pip install geopy --upgrade`
- the rdflib library, to build the RDF knowledge graph: `pip install rdflib`
- the owlready2 library, to create the ontology: `pip install owlready2`

A GeoNames account is also required to retreive the respective information.

[Protégé](https://protege.stanford.edu/) and [GraphDB](https://graphdb.ontotext.com/) were also used to test the validity of our ontology.

Finally, it is recommended to execute the GUI-related cells of code locally, in order to ensure that pop-up windows are enabled.
